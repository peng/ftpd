module goftp.io/ftpd

go 1.12

require (
	gitea.com/goftp/leveldb-auth v0.0.0-20190711092309-e8e3d5ad5ac8
	gitea.com/goftp/leveldb-perm v0.0.0-20190711092750-00b79e6da99c
	gitea.com/goftp/qiniu-driver v0.0.0-20191027083326-6e505f23c4f0
	gitea.com/lunny/tango v0.6.1
	gitea.com/tango/binding v0.0.0-20200204091933-f90d5bac28d2
	gitea.com/tango/flash v0.0.0-20190606021323-2b17fd0aed7c
	gitea.com/tango/renders v0.0.0-20191027160057-78fc56203eb4
	gitea.com/tango/session v0.0.0-20190606020146-89f560e05167
	gitea.com/tango/xsrf v0.0.0-20190606015726-fb1b2fb84238
	github.com/lunny/log v0.0.0-20160921050905-7887c61bf0de
	github.com/shurcooL/httpfs v0.0.0-20190707220628-8d4bc4ba7749 // indirect
	github.com/shurcooL/vfsgen v0.0.0-20181202132449-6a9ea43bcacd
	github.com/syndtr/goleveldb v1.0.0
	github.com/unknwon/goconfig v0.0.0-20190425194916-3dba17dd7b9e
	goftp.io/server v0.3.5-0.20200428022247-5cd49dc54bdb
)
